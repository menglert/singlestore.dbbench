#!/bin/bash

for i in `find /config/ -name "*.ini" -type f`; do
    dbbench --host=$S2_HOST \
        --port=$S2_PORT \
        --username=$S2_USER \
        --password=$S2_PWD \
        --database=$S2_DB \
        --intermediate-stats=$INTERIM_STATS \
        --query-stats-file=${i%%.ini}.csv \
        $i
done