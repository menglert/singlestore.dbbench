FROM golang
LABEL maintainer="Michael Englert <michi.e@tuta.io>"

RUN go install github.com/memsql/dbbench@latest

ADD run.sh /run.sh

CMD [ "/bin/bash", "-c", "/run.sh" ]